<?php

$dreamhost = 10.95;
$domain = 11.95;

$yearlyDr = $dreamhost * 12;
$yearlyD = $domain + $domain;

$grandTotal = $yearlyD + $yearlyDr;

include('inc/header.php');

?>

  <body>

    <table>
      <caption>Yearly Spendings</caption>
      <thead>
        <tr>
          <th scope="col">Months</th>
          <th scope="col">Dreamhost</th>
          <th scope="col">Domains</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td colspan="3">Grand Total: <?php echo $grandTotal; ?></td>
        </tr>
      </tfoot>
      <tbody>
        <tr>
          <th scope="row">Jan</th>
          <td><?php echo $dreamhost; ?></td>
          <td><?php echo $domain; ?></td>
        </tr>
        <tr>
          <th scope="row">Feb</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Mar</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Apr</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">May</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Jun</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Jul</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Aug</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Sept</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Oct</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Nov</th>
          <td><?php echo $dreamhost; ?></td>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Dec</th>
          <td><?php echo $dreamhost; ?></td>
          <td><?php echo $domain; ?></td>
        </tr>
      </tbody>
    </table>

  </body>
</html>
